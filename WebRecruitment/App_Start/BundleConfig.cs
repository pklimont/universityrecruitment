﻿using System.Web;
using System.Web.Optimization;

namespace WebRecruitment
{
    public class BundleConfig
    {
        // Aby uzyskać więcej informacji o grupowaniu, odwiedź stronę https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Użyj wersji deweloperskiej biblioteki Modernizr do nauki i opracowywania rozwiązań. Następnie, kiedy wszystko będzie
            // gotowe do produkcji, użyj narzędzia do kompilowania ze strony https://modernizr.com, aby wybrać wyłącznie potrzebne testy.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/popper.js",
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"
                      ));

           

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content/bootstrap-social.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css/bootstrap").
                Include("~/Content/bootstrap.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/Styles")
                .Include("~/bundles/styles.*"));

            bundles.Add(new ScriptBundle("~/Script/Bundles")
                .Include(
                    "~/bundles/inline.*",
                    "~/bundles/polyfills.*",
                    "~/bundles/scripts.*",
                    "~/bundles/vendor.*",
                    "~/bundles/main.*"));
            

            bundles.Add(new StyleBundle("~/Content/Styles")
                .Include("~/bundles/styles.*"));
        }
    }
}
