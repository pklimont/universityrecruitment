﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Model;

namespace WebRecruitment.Services.Interfaces
{
    public interface IUserService
    {
      ApplicationUser ValidateUser(string email, string password);

    }
}
