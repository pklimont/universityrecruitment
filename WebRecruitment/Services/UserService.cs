﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DAL;
using DAL.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using WebRecruitment.Helpers;
using WebRecruitment.Services.Interfaces;

namespace WebRecruitment.Services
{
    public class UserService:IUserService
    {
        private static RecruitmentDBContext db = new RecruitmentDBContext();
        private UserManager<ApplicationUser> userManager =
            new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

        public ApplicationUser ValidateUser(string email, string password)
        {
            string passwordHash = "";
            var passwordHasher = new CustomPasswordHasher();
            ApplicationUser user = userManager.FindByEmail(email);
            passwordHash = user.PasswordHash;
            PasswordVerificationResult status = passwordHasher.VerifyHashedPassword(passwordHash,password);
            if (status == PasswordVerificationResult.Success)
                return user;
            else return null;
        }

        


    }
}