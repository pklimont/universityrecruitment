"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var home_component_1 = require("./home/home.component");
var user_component_1 = require("./user/user.component");
var sign_up_component_1 = require("./user/sign-up/sign-up.component");
var sign_in_component_1 = require("./user/sign-in/sign-in.component");
var auth_guard_1 = require("./auth/auth.guard");
var demo_component_1 = require("./demo/demo.component");
exports.appRoutes = [
    { path: 'home', component: home_component_1.HomeComponent, canActivate: [auth_guard_1.AuthGuard] },
    {
        path: 'signup', component: user_component_1.UserComponent,
        children: [{ path: '', component: sign_up_component_1.SignUpComponent }]
    },
    {
        path: 'login', component: user_component_1.UserComponent,
        children: [{ path: '', component: sign_in_component_1.SignInComponent }]
    },
    {
        path: 'social', component: demo_component_1.DemoComponent,
    },
];
//# sourceMappingURL=routes.js.map