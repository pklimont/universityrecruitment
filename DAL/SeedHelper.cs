﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Model;
using FizzWare.NBuilder;

namespace DAL
{
    public class SeedHelper
    {
        public static void Seed(RecruitmentDBContext context,int countOfCandidates)
        {

            var daysGenerator = new RandomGenerator();

            var candidates = Builder<Candidate>.CreateListOfSize(countOfCandidates)
                .All()
                .With(c => c.FirstName = Faker.Name.First())
                .With(c => c.LastName = Faker.Name.Last())
                .With(c => c.DateOfBirth = DateTime.Now.AddDays(-daysGenerator.Next(366*18, 366*26)))
                .With(c=>c.FathersFirstName=Faker.Name.First())
                .With(c=>c.MothersMaidenName=Faker.Name.Last())
                .Build();

            context.Candidates.AddOrUpdate(c => c.Id, candidates.ToArray());
        }

    }
}
