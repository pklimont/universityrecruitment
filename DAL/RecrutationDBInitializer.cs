﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class RecruitmentDBInitializer : CreateDatabaseIfNotExists<RecruitmentDBContext>
    {
        protected override void Seed(RecruitmentDBContext context)
        {
            SeedHelper.Seed(context,10000);

            base.Seed(context);
        }
    }
}
