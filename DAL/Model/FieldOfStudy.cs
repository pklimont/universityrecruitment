﻿using System;

namespace DAL.Model
{
    public class FieldOfStudy:Entity<Guid>
    {
        public string Name { get; set; }
        public string StudiesDegree { get; set; }
        public string GraduateTitle { get; set; }
        public University University { get; set; }
        
    }
}