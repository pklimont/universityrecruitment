﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;

namespace DAL.Model
{
    public class Department:Entity<Guid>
    {
        public string Name { get; set; }
        public ICollection<FieldOfStudy> FieldsOfStudy { get; set; }


    }
}