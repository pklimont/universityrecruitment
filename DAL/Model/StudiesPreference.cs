﻿using System;

namespace DAL.Model
{
    public class StudiesPreference:Entity<Guid>
    {
        public FieldOfStudy FieldOfStudy { get; set; }
        public int OrderOfPreference { get; set; }
    }
}