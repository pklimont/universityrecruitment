﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class University:Entity<Guid>
    {
        public string Name { get; set; }
        public virtual ICollection<Department> Departments { get; set; }



    }
}
