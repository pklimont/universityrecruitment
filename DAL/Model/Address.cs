﻿using System;

namespace DAL.Model
{
    public class Address:Entity<Guid>   
    {
        public TypeOfAddress TypeOfAddress { get; set; }
        public string Country { get; set; }
        public string Department { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string Locale { get; set; }
        
    }

    


    public enum TypeOfAddress
    {
        AddressOfBirth,AddressOfLiving,AddressOfTemporaryRegistration
    }


}