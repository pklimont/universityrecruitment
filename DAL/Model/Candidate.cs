﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class Candidate:Entity<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string FathersFirstName { get; set; }
        public string MothersMaidenName { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public SchoolCertificate SchoolCertificate { get; set; }
        public virtual ICollection<StudiesPreference> StudiesPreferences { get; set; }

    }


    
}
