﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class SchoolCertificate:Entity<Guid>
    {
        public string SchoolName { get; set; }
        public ICollection<SubjectNote> SubjectNotes { get; set; }


    }
}
