﻿using System;

namespace DAL.Model
{
    public class SubjectNote:Entity<Guid>
    {
        public string SubjectName { get; set; }
        public int Note { get; set; }
        public bool IsTakenOnFinalExams { get; set; }
    }
}