﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class Entity<TPrimaryKey> where TPrimaryKey:struct 
    {
        public virtual TPrimaryKey Id { get; set; }
        
    }
}
