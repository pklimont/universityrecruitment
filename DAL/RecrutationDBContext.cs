﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Model;
using FizzWare.NBuilder;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL
{
    public class RecruitmentDBContext:IdentityDbContext<ApplicationUser>
    {
            public RecruitmentDBContext() : base("RecruitmentDBConnection")
            {

                Database.SetInitializer<RecruitmentDBContext>(null);
            }

            public static RecruitmentDBContext Create()
            {
                return new RecruitmentDBContext();
            }

            public virtual IDbSet<Address> Addresses { get; set; }
            public virtual IDbSet<Candidate> Candidates { get; set; }
            public virtual IDbSet<Department> Departments { get; set; }
            public virtual IDbSet<FieldOfStudy> FieldsOfStudies { get; set; }
            public virtual IDbSet<SchoolCertificate> SchoolCertificates { get; set; }
            public virtual IDbSet<StudiesPreference> StudiesPreferences { get; set; }
            public virtual IDbSet<SubjectNote> SubjectNotes { get; set; }
            public virtual IDbSet<University> Universities { get; set; }
            
    }


    
}
