namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TypeOfAddress = c.Int(nullable: false),
                        Country = c.String(),
                        Department = c.String(),
                        City = c.String(),
                        Street = c.String(),
                        HouseNumber = c.String(),
                        Locale = c.String(),
                        Candidate_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Candidates", t => t.Candidate_Id)
                .Index(t => t.Candidate_Id);
            
            CreateTable(
                "dbo.Candidates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        FathersFirstName = c.String(),
                        MothersMaidenName = c.String(),
                        SchoolCertificate_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SchoolCertificates", t => t.SchoolCertificate_Id)
                .Index(t => t.SchoolCertificate_Id);
            
            CreateTable(
                "dbo.SchoolCertificates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SchoolName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubjectNotes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SubjectName = c.String(),
                        Note = c.Int(nullable: false),
                        IsTakenOnFinalExams = c.Boolean(nullable: false),
                        SchoolCertificate_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SchoolCertificates", t => t.SchoolCertificate_Id)
                .Index(t => t.SchoolCertificate_Id);
            
            CreateTable(
                "dbo.StudiesPreferences",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OrderOfPreference = c.Int(nullable: false),
                        FieldOfStudy_Id = c.Guid(),
                        Candidate_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FieldOfStudies", t => t.FieldOfStudy_Id)
                .ForeignKey("dbo.Candidates", t => t.Candidate_Id)
                .Index(t => t.FieldOfStudy_Id)
                .Index(t => t.Candidate_Id);
            
            CreateTable(
                "dbo.FieldOfStudies",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        StudiesDegree = c.String(),
                        GraduateTitle = c.String(),
                        Department_Id = c.Guid(),
                        University_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.Department_Id)
                .ForeignKey("dbo.Universities", t => t.University_Id)
                .Index(t => t.Department_Id)
                .Index(t => t.University_Id);
            
            CreateTable(
                "dbo.Universities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        University_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Universities", t => t.University_Id)
                .Index(t => t.University_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.StudiesPreferences", "Candidate_Id", "dbo.Candidates");
            DropForeignKey("dbo.StudiesPreferences", "FieldOfStudy_Id", "dbo.FieldOfStudies");
            DropForeignKey("dbo.FieldOfStudies", "University_Id", "dbo.Universities");
            DropForeignKey("dbo.Departments", "University_Id", "dbo.Universities");
            DropForeignKey("dbo.FieldOfStudies", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.Candidates", "SchoolCertificate_Id", "dbo.SchoolCertificates");
            DropForeignKey("dbo.SubjectNotes", "SchoolCertificate_Id", "dbo.SchoolCertificates");
            DropForeignKey("dbo.Addresses", "Candidate_Id", "dbo.Candidates");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Departments", new[] { "University_Id" });
            DropIndex("dbo.FieldOfStudies", new[] { "University_Id" });
            DropIndex("dbo.FieldOfStudies", new[] { "Department_Id" });
            DropIndex("dbo.StudiesPreferences", new[] { "Candidate_Id" });
            DropIndex("dbo.StudiesPreferences", new[] { "FieldOfStudy_Id" });
            DropIndex("dbo.SubjectNotes", new[] { "SchoolCertificate_Id" });
            DropIndex("dbo.Candidates", new[] { "SchoolCertificate_Id" });
            DropIndex("dbo.Addresses", new[] { "Candidate_Id" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Departments");
            DropTable("dbo.Universities");
            DropTable("dbo.FieldOfStudies");
            DropTable("dbo.StudiesPreferences");
            DropTable("dbo.SubjectNotes");
            DropTable("dbo.SchoolCertificates");
            DropTable("dbo.Candidates");
            DropTable("dbo.Addresses");
        }
    }
}
